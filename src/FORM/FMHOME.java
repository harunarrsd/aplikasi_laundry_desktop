package FORM;

import eu.hansolo.clock.AnalogClock;
import eu.hansolo.clock.Luminosity;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.Timer;

/**
 *
 * @author Harun Ar
 */
public class FMHOME extends javax.swing.JInternalFrame {

    private boolean visible = false;
    private AnalogClock analogClock;
    
    public FMHOME() {
        initComponents();
        this.visible = false;
        setTanggal();
        setJam();
        AnalogClock();
    }
    
    public boolean getVisible(){
        return visible;
    }
    
    public void setStatusVisible(boolean visible){
        this.visible = visible;
    }
    
    public void setTanggal(){
        String nama_bulan="";
                String nama_hari="";
                String nol_hari="";
                Date dt=new Date();
                int nilai_tahun=dt.getYear()+1900;
                int nilai_bulan=dt.getMonth()+1;
                int nilai_hari=dt.getDate();
                int nilai_hari1=dt.getDay();
                if(nilai_hari1==1){
                    nama_hari="Senin";
                }else if(nilai_hari1==2){
                    nama_hari="Selasa";
                }else if(nilai_hari1==3){
                    nama_hari="Rabu";
                }else if(nilai_hari1==4){
                    nama_hari="Kamis";
                }else if(nilai_hari1==5){
                    nama_hari="Jum'at";
                }else if(nilai_hari1==6){
                    nama_hari="Sabtu";
                }else if(nilai_hari1==7){
                    nama_hari="Minggu";
                }
                if(nilai_bulan==1){
                    nama_bulan="Januari";
                }else if(nilai_bulan==2){
                    nama_bulan="Februari";
                }else if(nilai_bulan==2){
                    nama_bulan="Februari";
                }else if(nilai_bulan==3){
                    nama_bulan="Maret";
                }else if(nilai_bulan==4){
                    nama_bulan="April";
                }else if(nilai_bulan==5){
                    nama_bulan="Mei";
                }else if(nilai_bulan==6){
                    nama_bulan="Juni";
                }else if(nilai_bulan==7){
                    nama_bulan="Juli";
                }else if(nilai_bulan==8){
                    nama_bulan="Agustus";
                }else if(nilai_bulan==9){
                    nama_bulan="September";
                }else if(nilai_bulan==10){
                    nama_bulan="Oktober";
                }else if(nilai_bulan==11){
                    nama_bulan="November";
                }else if(nilai_bulan==12){
                    nama_bulan="Desember";
                }
                if(nilai_hari<=9){
                    nol_hari="0";
                }
                String bulan = nama_bulan;
                String hari1 = nama_hari;
                String hari = nol_hari+Integer.toString(nilai_hari);
                lbltgl.setText(hari1+", "+hari+" "+bulan+" "+nilai_tahun);
    }
    
    public void setJam(){
        ActionListener taskPerformer = new ActionListener() {

    public void actionPerformed(ActionEvent evt) {
    
    String nol_jam = "", nol_menit = "",nol_detik = "";

    java.util.Date dateTime = new java.util.Date();
    int nilai_jam = dateTime.getHours();
    int nilai_menit = dateTime.getMinutes();
    int nilai_detik = dateTime.getSeconds();

    if(nilai_jam <= 9) nol_jam= "0";
    if(nilai_menit <= 9) nol_menit= "0";
    if(nilai_detik <= 9) nol_detik= "0";

        String waktu = nol_jam + Integer.toString(nilai_jam);
        String menit = nol_menit + Integer.toString(nilai_menit);
        String detik = nol_detik + Integer.toString(nilai_detik);

        lblwaktu.setText(waktu+":"+menit+":"+detik+"");
        }
    };
    new Timer(1000, taskPerformer).start();
}
    
    public void AnalogClock() {
	analogClock = new AnalogClock();
	analogClock.setLuminosity(Luminosity.LIGHT);
	analogClock.setBounds(565, 0, 170, 300);
	analogclock.add(analogClock);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbltgl = new javax.swing.JLabel();
        lblwaktu = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        analogclock = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/5 a.png"))); // NOI18N
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameIconified(evt);
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        lbltgl.setFont(new java.awt.Font("Bernard MT Condensed", 0, 24)); // NOI18N

        lblwaktu.setFont(new java.awt.Font("Bernard MT Condensed", 0, 24)); // NOI18N
        lblwaktu.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        jLabel1.setFont(new java.awt.Font("Bernard MT Condensed", 0, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Selamat Datang");
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout analogclockLayout = new javax.swing.GroupLayout(analogclock);
        analogclock.setLayout(analogclockLayout);
        analogclockLayout.setHorizontalGroup(
            analogclockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        analogclockLayout.setVerticalGroup(
            analogclockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 170, Short.MAX_VALUE)
        );

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/2 a.png"))); // NOI18N

        jLabel3.setFont(new java.awt.Font("Bernard MT Condensed", 0, 36)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Di");

        jLabel4.setFont(new java.awt.Font("Bernard MT Condensed", 0, 36)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Aplikasi Laundry Yoo!");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbltgl, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 149, Short.MAX_VALUE)
                        .addComponent(lblwaktu, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(analogclock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblwaktu, javax.swing.GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE)
                    .addComponent(lbltgl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addGap(18, 18, 18)
                .addComponent(analogclock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        // TODO add your handling code here:
        setStatusVisible(false);
    }//GEN-LAST:event_formInternalFrameClosing

    private void formInternalFrameIconified(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameIconified
        // TODO add your handling code here:
        setStatusVisible(true);
    }//GEN-LAST:event_formInternalFrameIconified


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel analogclock;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel lbltgl;
    private javax.swing.JLabel lblwaktu;
    // End of variables declaration//GEN-END:variables
}
