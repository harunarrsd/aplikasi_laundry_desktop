package FORM;

import DATABASE.KONEKSI;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 *
 * @author Harun Ar
 */
public class FMTAMBAH extends javax.swing.JInternalFrame {

    private Statement st;
    public Connection con;
    private ResultSet rs;
    
    private boolean visible = false;
    
    public FMTAMBAH() {
        initComponents();
        this.visible = false;
        setTanggal();
        setJam();
        tambah();
        tambahJasa();
        tambahOrder();
        tambahTransaksi();
        setTanggalTransaksi();
        hakAkses(true);
    }

    public boolean getVisible(){
        return visible;
    }
    
    public void setStatusVisible(boolean visible){
        this.visible = visible;
    }
    
    public void setTanggal(){
                String nama_bulan="";
                String nama_hari="";
                String nol_hari="";
                Date dt=new Date();
                int nilai_tahun=dt.getYear()+1900;
                int nilai_bulan=dt.getMonth()+1;
                int nilai_hari=dt.getDate();
                int nilai_hari1=dt.getDay();
                if(nilai_hari1==1){
                    nama_hari="Senin";
                }else if(nilai_hari1==2){
                    nama_hari="Selasa";
                }else if(nilai_hari1==3){
                    nama_hari="Rabu";
                }else if(nilai_hari1==4){
                    nama_hari="Kamis";
                }else if(nilai_hari1==5){
                    nama_hari="Jum'at";
                }else if(nilai_hari1==6){
                    nama_hari="Sabtu";
                }else if(nilai_hari1==7){
                    nama_hari="Minggu";
                }
                if(nilai_bulan==1){
                    nama_bulan="Januari";
                }else if(nilai_bulan==2){
                    nama_bulan="Februari";
                }else if(nilai_bulan==2){
                    nama_bulan="Februari";
                }else if(nilai_bulan==3){
                    nama_bulan="Maret";
                }else if(nilai_bulan==4){
                    nama_bulan="April";
                }else if(nilai_bulan==5){
                    nama_bulan="Mei";
                }else if(nilai_bulan==6){
                    nama_bulan="Juni";
                }else if(nilai_bulan==7){
                    nama_bulan="Juli";
                }else if(nilai_bulan==8){
                    nama_bulan="Agustus";
                }else if(nilai_bulan==9){
                    nama_bulan="September";
                }else if(nilai_bulan==10){
                    nama_bulan="Oktober";
                }else if(nilai_bulan==11){
                    nama_bulan="November";
                }else if(nilai_bulan==12){
                    nama_bulan="Desember";
                }
                if(nilai_hari<=9){
                    nol_hari="0";
                }
                String bulan = nama_bulan;
                String hari1 = nama_hari;
                String hari = nol_hari+Integer.toString(nilai_hari);
                lbltgl.setText(hari1+", "+hari+" "+bulan+" "+nilai_tahun);
    }
    
public void setJam(){
        ActionListener taskPerformer = new ActionListener() {

    public void actionPerformed(ActionEvent evt) {
    
    String nol_jam = "", nol_menit = "",nol_detik = "";

    java.util.Date dateTime = new java.util.Date();
    int nilai_jam = dateTime.getHours();
    int nilai_menit = dateTime.getMinutes();
    int nilai_detik = dateTime.getSeconds();

    if(nilai_jam <= 9) nol_jam= "0";
    if(nilai_menit <= 9) nol_menit= "0";
    if(nilai_detik <= 9) nol_detik= "0";

        String waktu = nol_jam + Integer.toString(nilai_jam);
        String menit = nol_menit + Integer.toString(nilai_menit);
        String detik = nol_detik + Integer.toString(nilai_detik);

        lblwaktu.setText(waktu+":"+menit+":"+detik+"");
        }
    };
    new Timer(1000, taskPerformer).start();
}

private void hakAkses(boolean akses) {
    btnTambah.setEnabled(!akses);
        btnSimpan.setEnabled(akses);
        btnUbah.setEnabled(!akses);
        btnHapus.setEnabled(!akses);
        btnCari.setEnabled(akses);
    btnTambahJ.setEnabled(!akses);
        btnSimpanJ.setEnabled(akses);
        btnUbahJ.setEnabled(!akses);
        btnHapusJ.setEnabled(!akses);
        btnCariJ.setEnabled(akses);
    btnTambahO.setEnabled(!akses);
        btnSimpanO.setEnabled(akses);
        btnUbahO.setEnabled(!akses);
        btnHapusO.setEnabled(!akses);
        btnCariO.setEnabled(akses);
    btnTambahT.setEnabled(!akses);
        btnSimpanT.setEnabled(akses);
        btnUbahT.setEnabled(!akses);
        btnHapusT.setEnabled(!akses);
        btnCariT.setEnabled(akses);
    }

// <editor-fold defaultstate="collapsed" desc="Pelanggan">
private String getKodePelanggan() {
        String noTransaksi = "";
        String query;
        query = "SELECT MAX(no) AS no FROM pelanggan ";
        Connection connection;
        Statement statement;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            if(rs.next()) {
                int noTerbesar = rs.getInt("no") + 1;
                noTransaksi = "KDP.234.0" + noTerbesar;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
        return noTransaksi;
    }

public void tambah(){
    textkodepelanggan.setText(getKodePelanggan());
        textnamapelanggan.setText("");
        textalamat.setText("");
        textnotelp.setText("");
        cmbstatus.setSelectedIndex(-1);
        hakAkses(true);
    }

public void savePelanggan(){
        String query;
        query = ("insert into pelanggan values (null, ?, ?, ?, ?, ?)");
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textkodepelanggan.getText());
            statement.setString(2, textnamapelanggan.getText());
            statement.setString(3, textalamat.getText());
            statement.setString(4, textnotelp.getText());
            statement.setString(5, cmbstatus.getSelectedItem().toString());
            int hasil = statement.executeUpdate();
            if(hasil==1){
                JOptionPane.showMessageDialog(this, "Data sukses di simpan");
                tambah();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

private void cari (){
        try {
            String pertanyaan = "Masukan Kode Pelanggan yang Anda cari";
            String kodepelanggan = JOptionPane.showInputDialog(this, pertanyaan);        
            if(!kodepelanggan.trim().equals("")){
                String query = "SELECT * FROM pelanggan WHERE kode_pelanggan = ?";
                PreparedStatement statement;
                Connection connection;
                try {
                    connection = KONEKSI.koneksiDatabase();
                    statement = connection.prepareCall(query);
                    statement.setString(1, kodepelanggan);
                    rs = statement.executeQuery();
                    if(rs.next()){
                        textkodepelanggan.setText(rs.getString("kode_pelanggan"));
                        textnamapelanggan.setText(rs.getString("nama_pelanggan"));
                        textalamat.setText(rs.getString("alamat_pelanggan"));
                        textnotelp.setText(rs.getString("notelp"));
                        cmbstatus.setSelectedItem(rs.getString("status_pelanggan"));
                        hakAkses(false);
                    } else{
                        JOptionPane.showMessageDialog(this, "Data Barang Tidak ada");
                        tambah();
                    }
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(this, e.getMessage());
                }
            }
        } catch (HeadlessException e) {
        }
    }
    
private void hapus(){
        String query;
        query = "DELETE FROM pelanggan WHERE kode_pelanggan = ?";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareCall(query);
            statement.setString(1, textkodepelanggan.getText());
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data Barang Telah Dihapus");
                tambah();
            }
        } catch (SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

private void ubah(){
        String query;
        query = "UPDATE pelanggan SET kode_pelanggan=?,nama_pelanggan=?,alamat_pelanggan=?,notelp=?,status_pelanggan=?"
                + "WHERE kode_pelanggan ='"+textkodepelanggan.getText()+"'";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textkodepelanggan.getText());
            statement.setString(2, textnamapelanggan.getText());
            statement.setString(3, textalamat.getText());
            statement.setString(4, textnotelp.getText());
            statement.setString(5, cmbstatus.getSelectedItem().toString());
            int hasil = statement.executeUpdate();
            if (hasil == 1){
                   JOptionPane.showMessageDialog(this, "Data Barang Telah Diubah");
                   tambah();
            }
            } catch (SQLException e){
                    JOptionPane.showMessageDialog(this, e.getMessage());
                    }
        }
//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="Jasa">
private String getKodeJasa() {
        String noTransaksi = "";
        String query;
        query = "SELECT MAX(no) AS no FROM jasa ";
        Connection connection;
        Statement statement;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            if(rs.next()) {
                int noTerbesar = rs.getInt("no") + 1;
                noTransaksi = "KDJS.234.0" + noTerbesar;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
        return noTransaksi;
    }

public void tambahJasa(){
    textkodejasa.setText(getKodeJasa());
        cmbnamajasa.setSelectedIndex(-1);
        cmbsatuan.setSelectedIndex(0);
        textharga.setText("");
        hakAkses(true);
    }

public void saveJasa(){
        String query;
        query = ("insert into jasa values (null, ?, ?, ?, ?)");
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textkodejasa.getText());
            statement.setString(2, cmbnamajasa.getSelectedItem().toString());
            statement.setString(3, cmbsatuan.getSelectedItem().toString());
            statement.setString(4, textharga.getText());
            int hasil = statement.executeUpdate();
            if(hasil==1){
                JOptionPane.showMessageDialog(this, "Data sukses di simpan");
                tambahJasa();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

private void cariJasa(){
        try {
            String pertanyaan = "Masukan Kode Jasa yang Anda cari";
            String kodejasa = JOptionPane.showInputDialog(this, pertanyaan);        
            if(!kodejasa.trim().equals("")){
                String query = "SELECT * FROM jasa WHERE kode_jasa = ?";
                PreparedStatement statement;
                Connection connection;
                try {
                    connection = KONEKSI.koneksiDatabase();
                    statement = connection.prepareCall(query);
                    statement.setString(1, kodejasa);
                    rs = statement.executeQuery();
                    if(rs.next()){
                        textkodejasa.setText(rs.getString("kode_jasa"));
                        cmbnamajasa.setSelectedItem(rs.getString("nama_jasa"));
                        cmbsatuan.setSelectedItem(rs.getString("satuan"));
                        textharga.setText(rs.getString("harga"));
                        hakAkses(false);
                    } else{
                        JOptionPane.showMessageDialog(this, "Data Barang Tidak ada");
                        tambahJasa();
                    }
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(this, e.getMessage());
                }
            }
        } catch (HeadlessException e) {
        }
    }

private void hapusJasa(){
        String query;
        query = "DELETE FROM jasa WHERE kode_jasa = ?";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareCall(query);
            statement.setString(1, textkodejasa.getText());
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data Barang Telah Dihapus");
                tambahJasa();
            }
        } catch (SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

private void ubahJasa(){
        String query;
        query = "UPDATE jasa SET kode_jasa=?,nama_jasa=?,satuan=?,harga=?"
                + "WHERE kode_jasa ='"+textkodejasa.getText()+"'";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textkodejasa.getText());
            statement.setString(2, cmbnamajasa.getSelectedItem().toString());
            statement.setString(3, cmbsatuan.getSelectedItem().toString());
            statement.setString(4, textharga.getText());
            int hasil = statement.executeUpdate();
            if (hasil == 1){
                   JOptionPane.showMessageDialog(this, "Data Barang Telah Diubah");
                   tambahJasa();
            }
            } catch (SQLException e){
                    JOptionPane.showMessageDialog(this, e.getMessage());
                    }
        }
//</editor-fold>

// <editor-fold defaultstate="collapsed" desc="Pesanan Baru">
private String getNoOrder() {
        String noTransaksi = "";
        String query;
        query = "SELECT MAX(no) AS no FROM order_pesanan ";
        Connection connection;
        Statement statement;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            if(rs.next()) {
                int noTerbesar = rs.getInt("no") + 1;
                noTransaksi = "LYO.234.0" + noTerbesar;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
        return noTransaksi;
    }

public void tambahOrder(){
    textNoOrder.setText(getNoOrder());
    dtTglAwal.setDate(null);
    dtTglAkhir.setDate(null);
    textJumlah.setText("");
    cmbkodepewangi.setSelectedIndex(-1);
    textpewangi.setText("");
    textkodepelangganorder.setText("");
    textnamapelangganorder.setText("");
    hakAkses(true);
    }

public void saveOrder(){
    String tampil ="dd-MM-yyyy";
        SimpleDateFormat fm = new SimpleDateFormat(tampil);
        String tglawal = String.valueOf(fm.format(dtTglAwal.getDate()));
        String tglakhir = String.valueOf(fm.format(dtTglAkhir.getDate()));
        
        String query;
        query = ("insert into order_pesanan values (null, ?, ?, ?, ?, ?, ?, ?)");
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textNoOrder.getText());
            statement.setString(2, tglawal);
            statement.setString(3, tglakhir);
            statement.setString(4, textJumlah.getText());
            statement.setString(5, textpewangi.getText());
            statement.setString(6, textkodepelangganorder.getText());
            statement.setString(7, textnamapelangganorder.getText());
            int hasil = statement.executeUpdate();
            if(hasil==1){
                JOptionPane.showMessageDialog(this, "Data sukses di simpan");
                tambahOrder();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

private void cariOrder(){
        try {
            String pertanyaan = "Masukan No Order yang Anda cari";
            String noorder = JOptionPane.showInputDialog(this, pertanyaan);        
            if(!noorder.trim().equals("")){
                String query = "SELECT * FROM order_pesanan WHERE no_order = ?";
                PreparedStatement statement;
                Connection connection;
                try {
                    connection = KONEKSI.koneksiDatabase();
                    statement = connection.prepareCall(query);
                    statement.setString(1, noorder);
                    rs = statement.executeQuery();
                    if(rs.next()){
                        textNoOrder.setText(rs.getString("no_order"));
                        dtTglAwal.setDate(rs.getDate("tgl_order"));
                        dtTglAkhir.setDate(rs.getDate("tgl_rencana_selesai"));
                        textJumlah.setText(rs.getString("jumlah"));
                        textpewangi.setText(rs.getString("nama_pewangi"));
                        textkodepelangganorder.setText(rs.getString("kode_pelanggan"));
                        textnamapelangganorder.setText(rs.getString("nama_pelanggan"));
                        hakAkses(false);
                    } else{
                        JOptionPane.showMessageDialog(this, "Data Barang Tidak ada");
                        tambahJasa();
                    }
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(this, e.getMessage());
                }
            }
        } catch (HeadlessException e) {
        }
    }

private void hapusOrder(){
        String query;
        query = "DELETE FROM order_pesanan WHERE no_order = ?";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareCall(query);
            statement.setString(1, textNoOrder.getText());
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data Barang Telah Dihapus");
                tambahOrder();
            }
        } catch (SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

private void ubahOrder(){
    String tampil ="dd-MM-yyyy";
        SimpleDateFormat fm = new SimpleDateFormat(tampil);
        String tglawal = String.valueOf(fm.format(dtTglAwal.getDate()));
        String tglakhir = String.valueOf(fm.format(dtTglAkhir.getDate()));
        
        String query;
        query = "UPDATE order_pesanan SET no_order=?,tgl_order=?,tgl_rencana_selesai=?,jumlah=?,nama_pewangi=?,kode_pelanggan=?,nama_pelanggan=?"
                + "WHERE no_order ='"+textNoOrder.getText()+"'";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textNoOrder.getText());
            statement.setString(2, tglawal);
            statement.setString(3, tglakhir);
            statement.setString(4, textJumlah.getText());
            statement.setString(5, textpewangi.getText());
            statement.setString(6, textkodepelangganorder.getText());
            statement.setString(7, textnamapelangganorder.getText());
            int hasil = statement.executeUpdate();
            if (hasil == 1){
                   JOptionPane.showMessageDialog(this, "Data Barang Telah Diubah");
                   tambahOrder();
            }
            } catch (SQLException e){
                    JOptionPane.showMessageDialog(this, e.getMessage());
                    }
        }

private void cariPelanggan() {
        String query = "SELECT * FROM pelanggan WHERE kode_pelanggan = ?";
        PreparedStatement statement;
        Connection connection = KONEKSI.koneksiDatabase();
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, textkodepelangganorder.getText());
            ResultSet rs = statement.executeQuery();
            if(rs.next()) {
                textkodepelangganorder.setText(rs.getString("kode_pelanggan"));
                textnamapelangganorder.setText(rs.getString("nama_pelanggan"));
                btnSimpanO.requestFocus();
            } else {
                JOptionPane.showMessageDialog(this, "Data Barang Tidak ada");
                tambahOrder();
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Transaksi">
private String getNoTransaksi() {
        String noTransaksi = "";
        String query;
        query = "SELECT MAX(no) AS no FROM transaksi ";
        Connection connection;
        Statement statement;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            if(rs.next()) {
                int noTerbesar = rs.getInt("no") + 1;
                noTransaksi = "TL.234.0" + noTerbesar;
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
        return noTransaksi;
    }

public void tambahTransaksi(){
    textnotransaksi.setText(getNoTransaksi());
    textkodejasatransaksi.setText("");
    textnamajasatransaksi.setText("");
    textsatuantransaksi.setText("");
    texthargatransaksi.setText("");
    textnoordertransaksi.setText("");
    textjumlahtransaksi.setText("");
    texttotal.setText("");
    textbayar.setText("");
    textkembali.setText("");
    hakAkses(true);
    }

public void setTanggalTransaksi(){
        java.util.Date skrg = new java.util.Date();
        java.text.SimpleDateFormat kal = new
        java.text.SimpleDateFormat("dd/MM/yyyy");
        texttgltransaksi.setText(kal.format(skrg));
    }

public void saveTransaksi(){ 
        String query;
        query = ("insert into transaksi values (null, ?, ?, ?, ?, ?)");
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textnotransaksi.getText());
            statement.setString(2, texttgltransaksi.getText());
            statement.setString(3, textnoordertransaksi.getText());
            statement.setString(4, textbayar.getText());
            statement.setString(5, textkembali.getText());
            saveTransaksi2();
            int hasil = statement.executeUpdate();
            if(hasil==1){
                JOptionPane.showMessageDialog(this, "Data sukses di simpan");
                tambahTransaksi();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
public void saveTransaksi2(){
        
        String query;
        query = ("insert into transaksi_detail values (null, ?, ?, ?, ?, ?, ?, ?)");
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textnoordertransaksi.getText());
            statement.setString(2, textkodejasatransaksi.getText());
            statement.setString(3, textnamajasatransaksi.getText());
            statement.setString(4, textsatuantransaksi.getText());
            statement.setString(5, texthargatransaksi.getText());
            statement.setString(6, textjumlahtransaksi.getText());
            statement.setString(7, texttotal.getText());
            int hasil = statement.executeUpdate();
            if(hasil==1){
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

private void cariTransaksi(){
        try {
            String pertanyaan = "Masukan No Transaksi yang Anda cari";
            String noorder = JOptionPane.showInputDialog(this, pertanyaan);        
            if(!noorder.trim().equals("")){
                String query = "SELECT * FROM transaksi WHERE no_transaksi = ?";
                PreparedStatement statement;
                Connection connection;
                try {
                    connection = KONEKSI.koneksiDatabase();
                    statement = connection.prepareCall(query);
                    statement.setString(1, noorder);
                    rs = statement.executeQuery();
                    if(rs.next()){
                        textnotransaksi.setText(rs.getString("no_transaksi"));
                        texttgltransaksi.setText(rs.getString("tgl_transaksi"));
                        textnoordertransaksi.setText(rs.getString("no_order"));
                        textbayar.setText(rs.getString("dibayar"));
                        textkembali.setText(rs.getString("kembali"));
                        cariTransaksi2();
                        hakAkses(false);
                    } else{
                        JOptionPane.showMessageDialog(this, "Data Barang Tidak ada");
                        tambahTransaksi();
                    }
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(this, e.getMessage());
                }
            }
        } catch (HeadlessException e) {
        }
    }
private void cariTransaksi2(){
        try {
            String pertanyaan = "Masukan No Order yang Anda cari";
            String noagen = JOptionPane.showInputDialog(this, pertanyaan);        
            if(!noagen.trim().equals("")){
                String query = "SELECT * FROM transaksi_detail WHERE no_order = ?";
                PreparedStatement statement;
                Connection connection;
                try {
                    connection = KONEKSI.koneksiDatabase();
                    statement = connection.prepareCall(query);
                    statement.setString(1, noagen);
                    rs = statement.executeQuery();
                    if(rs.next()){
                        textnoordertransaksi.setText(rs.getString("no_order"));
                        textkodejasatransaksi.setText(rs.getString("kode_jasa"));
                        textnamajasatransaksi.setText(rs.getString("nama_jasa"));
                        textsatuantransaksi.setText(rs.getString("satuan"));
                        texthargatransaksi.setText(rs.getString("harga"));
                        textjumlahtransaksi.setText(rs.getString("jumlah"));
                        texttotal.setText(rs.getString("total"));
                        hakAkses(false);
                    } else{
                    }
                } catch (SQLException e) {
                    JOptionPane.showMessageDialog(this, e.getMessage());
                }
            }
        } catch (HeadlessException e) {
        }
    }

private void hapusTransaksi(){
        String query;
        query = "DELETE FROM transaksi WHERE no_transaksi = ?";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareCall(query);
            statement.setString(1, textnotransaksi.getText());
            hapusTransaksi2();
            int hasil = statement.executeUpdate();
            if(hasil == 1){
                JOptionPane.showMessageDialog(this, "Data Barang Telah Dihapus");
                tambahTransaksi();
            }
        } catch (SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }
private void hapusTransaksi2(){
        String query;
        query = "DELETE FROM transaksi_detail WHERE no_order = ?";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareCall(query);
            statement.setString(1, textnoordertransaksi.getText());
            int hasil = statement.executeUpdate();
            if(hasil == 1){

            }
        } catch (SQLException e){
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

private void ubahTransaksi(){
        String query;
        query = "UPDATE transaksi SET no_transaksi=?,tgl_transaksi=?,no_order=?,dibayar=?,kembali=?"
                + "WHERE no_transaksi ='"+textnotransaksi.getText()+"'";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textnotransaksi.getText());
            statement.setString(2, texttgltransaksi.getText());
            statement.setString(3, textnoordertransaksi.getText());
            statement.setString(4, textbayar.getText());
            statement.setString(5, textkembali.getText());
            ubahTransaksi2();
            int hasil = statement.executeUpdate();
            if (hasil == 1){
                   JOptionPane.showMessageDialog(this, "Data Barang Telah Diubah");
                   tambahTransaksi();
            }
            } catch (SQLException e){
                    JOptionPane.showMessageDialog(this, e.getMessage());
                    }
        }
private void ubahTransaksi2(){
        String query;
        query = "UPDATE transaksi_detail SET no_order=?,kode_jasa=?,nama_jasa=?,satuan=?,harga=?,jumlah=?,total=?"
                + "WHERE no_order ='"+textnoordertransaksi.getText()+"'";
        PreparedStatement statement;
        Connection connection;
        try {
            connection = KONEKSI.koneksiDatabase();
            statement = connection.prepareStatement(query);
            statement.setString(1, textnoordertransaksi.getText());
            statement.setString(2, textkodejasatransaksi.getText());
            statement.setString(3, textnamajasatransaksi.getText());
            statement.setString(4, textsatuantransaksi.getText());
            statement.setString(5, texthargatransaksi.getText());
            statement.setString(6, textjumlahtransaksi.getText());
            statement.setString(7, texttotal.getText());
            int hasil = statement.executeUpdate();
            if (hasil == 1){

            }
            } catch (SQLException e){
                    JOptionPane.showMessageDialog(this, e.getMessage());
                    }
        }

private void cariKodeJasa() {
        String query = "SELECT * FROM jasa WHERE kode_jasa = ?";
        PreparedStatement statement;
        Connection connection = KONEKSI.koneksiDatabase();
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, textkodejasatransaksi.getText());
            ResultSet rs = statement.executeQuery();
            if(rs.next()) {
                textkodejasatransaksi.setText(rs.getString("kode_jasa"));
                textnamajasatransaksi.setText(rs.getString("nama_jasa"));
                textsatuantransaksi.setText(rs.getString("satuan"));
                texthargatransaksi.setText(rs.getString("harga"));
                textnoordertransaksi.requestFocus();
            } else {
                JOptionPane.showMessageDialog(this, "Data Barang Tidak ada");
                tambahTransaksi();
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

private void cariNoOrder() {
        String query = "SELECT * FROM order_pesanan WHERE no_order = ?";
        PreparedStatement statement;
        Connection connection = KONEKSI.koneksiDatabase();
        try {
            statement = connection.prepareStatement(query);
            statement.setString(1, textnoordertransaksi.getText());
            ResultSet rs = statement.executeQuery();
            if(rs.next()) {
                textjumlahtransaksi.setText(rs.getString("jumlah"));
                textnoordertransaksi.requestFocus();
            } else {
                JOptionPane.showMessageDialog(this, "Data Barang Tidak ada");
                tambahOrder();
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
        }
    }

private void hitungTotal() {
        int harga = Integer.parseInt(texthargatransaksi.getText());
        int jumlah = Integer.parseInt(textjumlahtransaksi.getText());
        int total = harga * jumlah;
        texttotal.setText(String.valueOf(total));
    }

private void hitungKembali() {
        int total = Integer.parseInt(texttotal.getText());
        int uangBayar = Integer.parseInt(textbayar.getText());
        int kembali = uangBayar - total;
        textkembali.setText(String.valueOf(kembali));
    }
// </editor-fold>

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbltgl = new javax.swing.JLabel();
        lblwaktu = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        textkodepelanggan = new javax.swing.JTextField();
        textnamapelanggan = new javax.swing.JTextField();
        textnotelp = new javax.swing.JTextField();
        cmbstatus = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        textalamat = new javax.swing.JTextArea();
        jPanel5 = new javax.swing.JPanel();
        btnSimpan = new javax.swing.JButton();
        btnUbah = new javax.swing.JButton();
        btnHapus = new javax.swing.JButton();
        btnCari = new javax.swing.JButton();
        btnTambah = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        textkodejasa = new javax.swing.JTextField();
        cmbsatuan = new javax.swing.JComboBox();
        textharga = new javax.swing.JTextField();
        cmbnamajasa = new javax.swing.JComboBox();
        btnSimpanJ = new javax.swing.JButton();
        btnTambahJ = new javax.swing.JButton();
        btnUbahJ = new javax.swing.JButton();
        btnHapusJ = new javax.swing.JButton();
        btnCariJ = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        textNoOrder = new javax.swing.JTextField();
        textJumlah = new javax.swing.JTextField();
        textpewangi = new javax.swing.JTextField();
        textkodepelangganorder = new javax.swing.JTextField();
        cmbkodepewangi = new javax.swing.JComboBox();
        textnamapelangganorder = new javax.swing.JTextField();
        btnSimpanO = new javax.swing.JButton();
        btnTambahO = new javax.swing.JButton();
        btnUbahO = new javax.swing.JButton();
        btnHapusO = new javax.swing.JButton();
        btnCariO = new javax.swing.JButton();
        dtTglAwal = new com.toedter.calendar.JDateChooser();
        dtTglAkhir = new com.toedter.calendar.JDateChooser();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        textnotransaksi = new javax.swing.JTextField();
        texttgltransaksi = new javax.swing.JTextField();
        textkodejasatransaksi = new javax.swing.JTextField();
        textnamajasatransaksi = new javax.swing.JTextField();
        textsatuantransaksi = new javax.swing.JTextField();
        texthargatransaksi = new javax.swing.JTextField();
        textnoordertransaksi = new javax.swing.JTextField();
        textjumlahtransaksi = new javax.swing.JTextField();
        texttotal = new javax.swing.JTextField();
        textbayar = new javax.swing.JTextField();
        textkembali = new javax.swing.JTextField();
        btnSimpanT = new javax.swing.JButton();
        btnTambahT = new javax.swing.JButton();
        btnUbahT = new javax.swing.JButton();
        btnHapusT = new javax.swing.JButton();
        btnCariT = new javax.swing.JButton();

        setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));
        setFrameIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/3 a.png"))); // NOI18N
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameIconified(evt);
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        lbltgl.setFont(new java.awt.Font("Bernard MT Condensed", 0, 24)); // NOI18N

        lblwaktu.setFont(new java.awt.Font("Bernard MT Condensed", 0, 24)); // NOI18N
        lblwaktu.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        jTabbedPane1.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        jTabbedPane1.setTabPlacement(javax.swing.JTabbedPane.LEFT);
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Pelanggan");
        jLabel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel5.setText("Kode Pelanggan");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel6.setText("Nama Pelanggan");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel7.setText("Alamat Pelanggan");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel8.setText("No. Telp Pelanggan");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel9.setText("Status Pelanggan");

        textkodepelanggan.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textkodepelanggan.setEnabled(false);
        textkodepelanggan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textkodepelangganKeyTyped(evt);
            }
        });

        textnamapelanggan.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        textnotelp.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        cmbstatus.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        cmbstatus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Langganan", "Umum" }));
        cmbstatus.setSelectedIndex(-1);

        textalamat.setColumns(20);
        textalamat.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textalamat.setRows(5);
        jScrollPane1.setViewportView(textalamat);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 270, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 458, Short.MAX_VALUE)
        );

        btnSimpan.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnSimpan.setText("Simpan");
        btnSimpan.setContentAreaFilled(false);
        btnSimpan.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });

        btnUbah.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnUbah.setText("Ubah");
        btnUbah.setContentAreaFilled(false);
        btnUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUbahActionPerformed(evt);
            }
        });

        btnHapus.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnHapus.setText("Hapus");
        btnHapus.setContentAreaFilled(false);
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        btnCari.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnCari.setText("Cari");
        btnCari.setContentAreaFilled(false);
        btnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariActionPerformed(evt);
            }
        });

        btnTambah.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnTambah.setText("Tambah");
        btnTambah.setContentAreaFilled(false);
        btnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(38, 38, 38)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textnamapelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textkodepelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9))
                        .addGap(25, 25, 25)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbstatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textnotelp, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnSimpan)
                        .addGap(18, 18, 18)
                        .addComponent(btnTambah)
                        .addGap(18, 18, 18)
                        .addComponent(btnUbah, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnHapus)
                        .addGap(18, 18, 18)
                        .addComponent(btnCari)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(textkodepelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(textnamapelanggan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(textnotelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(cmbstatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                            .addComponent(btnSimpan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnUbah, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnHapus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnCari, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnTambah)))
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Pelanggan", jPanel1);

        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Jasa");
        jLabel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 323, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 296, Short.MAX_VALUE)
        );

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel10.setText("Kode Jasa");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel11.setText("Nama Jasa");

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel12.setText("Satuan");

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel13.setText("Harga");

        textkodejasa.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textkodejasa.setEnabled(false);

        cmbsatuan.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        cmbsatuan.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Kg", "Pcs" }));
        cmbsatuan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbsatuanActionPerformed(evt);
            }
        });

        textharga.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textharga.setEnabled(false);

        cmbnamajasa.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        cmbnamajasa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Cuci Kering", "Cuci Basah", "Cuci Setrika" }));
        cmbnamajasa.setSelectedIndex(-1);
        cmbnamajasa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbnamajasaActionPerformed(evt);
            }
        });

        btnSimpanJ.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnSimpanJ.setText("Simpan");
        btnSimpanJ.setContentAreaFilled(false);
        btnSimpanJ.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnSimpanJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanJActionPerformed(evt);
            }
        });

        btnTambahJ.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnTambahJ.setText("Tambah");
        btnTambahJ.setContentAreaFilled(false);
        btnTambahJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahJActionPerformed(evt);
            }
        });

        btnUbahJ.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnUbahJ.setText("Ubah");
        btnUbahJ.setContentAreaFilled(false);
        btnUbahJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUbahJActionPerformed(evt);
            }
        });

        btnHapusJ.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnHapusJ.setText("Hapus");
        btnHapusJ.setContentAreaFilled(false);
        btnHapusJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusJActionPerformed(evt);
            }
        });

        btnCariJ.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnCariJ.setText("Cari");
        btnCariJ.setContentAreaFilled(false);
        btnCariJ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariJActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13))
                        .addGap(28, 28, 28)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbsatuan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbnamajasa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textharga, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textkodejasa, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnSimpanJ)
                        .addGap(18, 18, 18)
                        .addComponent(btnTambahJ)
                        .addGap(18, 18, 18)
                        .addComponent(btnUbahJ)
                        .addGap(18, 18, 18)
                        .addComponent(btnHapusJ)
                        .addGap(18, 18, 18)
                        .addComponent(btnCariJ)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(textkodejasa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(cmbnamajasa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(cmbsatuan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(textharga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnSimpanJ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnTambahJ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnUbahJ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnHapusJ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnCariJ, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 173, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Jasa", jPanel2);

        jPanel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Pesanan Baru");
        jLabel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 273, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 360, Short.MAX_VALUE)
        );

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel15.setText("No. Order");

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel16.setText("Tanggal Order");

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel17.setText("Tanggal Rencana Selesai");

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel18.setText("Jumlah");

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel20.setText("Pewangi");

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel21.setText("Kode Pelanggan");

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel22.setText("Nama Pelanggan");

        textNoOrder.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textNoOrder.setEnabled(false);

        textJumlah.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        textpewangi.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textpewangi.setEnabled(false);

        textkodepelangganorder.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textkodepelangganorder.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textkodepelangganorderKeyTyped(evt);
            }
        });

        cmbkodepewangi.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        cmbkodepewangi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "001", "002", "003" }));
        cmbkodepewangi.setSelectedIndex(-1);
        cmbkodepewangi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbkodepewangiActionPerformed(evt);
            }
        });

        textnamapelangganorder.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textnamapelangganorder.setEnabled(false);

        btnSimpanO.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnSimpanO.setText("Simpan");
        btnSimpanO.setContentAreaFilled(false);
        btnSimpanO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanOActionPerformed(evt);
            }
        });

        btnTambahO.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnTambahO.setText("Tambah");
        btnTambahO.setContentAreaFilled(false);
        btnTambahO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahOActionPerformed(evt);
            }
        });

        btnUbahO.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnUbahO.setText("Ubah");
        btnUbahO.setContentAreaFilled(false);
        btnUbahO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUbahOActionPerformed(evt);
            }
        });

        btnHapusO.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnHapusO.setText("Hapus");
        btnHapusO.setContentAreaFilled(false);
        btnHapusO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusOActionPerformed(evt);
            }
        });

        btnCariO.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnCariO.setText("Cari");
        btnCariO.setContentAreaFilled(false);
        btnCariO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariOActionPerformed(evt);
            }
        });

        dtTglAwal.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        dtTglAkhir.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(btnSimpanO)
                        .addGap(18, 18, 18)
                        .addComponent(btnTambahO)
                        .addGap(18, 18, 18)
                        .addComponent(btnUbahO)
                        .addGap(18, 18, 18)
                        .addComponent(btnHapusO)
                        .addGap(18, 18, 18)
                        .addComponent(btnCariO))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addComponent(jLabel16)
                            .addComponent(jLabel15)
                            .addComponent(jLabel18)
                            .addComponent(jLabel20)
                            .addComponent(jLabel21)
                            .addComponent(jLabel22))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(cmbkodepewangi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(textpewangi, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(textJumlah, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dtTglAwal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dtTglAkhir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textkodepelangganorder, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textNoOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textnamapelangganorder, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel3)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel15)
                                    .addComponent(textNoOrder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jLabel16))
                            .addComponent(dtTglAwal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel17)
                            .addComponent(dtTglAkhir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(textJumlah, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20)
                            .addComponent(textpewangi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbkodepewangi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel21)
                            .addComponent(textkodepelangganorder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22)
                            .addComponent(textnamapelangganorder, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnSimpanO, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnTambahO)
                            .addComponent(btnUbahO, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnHapusO)
                                .addComponent(btnCariO))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(93, 93, 93)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Pesanan Baru", jPanel3);

        jPanel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Transaksi");
        jLabel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 97, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 212, Short.MAX_VALUE)
        );

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel24.setText("No Transaksi");

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel25.setText("Tanggal Transaksi");

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel26.setText("Kode Jasa");

        jLabel27.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel27.setText("Nama Jasa");

        jLabel28.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel28.setText("Satuan");

        jLabel29.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel29.setText("Harga");

        jLabel30.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel30.setText("No Order");

        jLabel31.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel31.setText("Jumlah");

        jLabel32.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel32.setText("Total");

        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel33.setText("Uang Bayar");

        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel34.setText("Kembali");

        textnotransaksi.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textnotransaksi.setEnabled(false);

        texttgltransaksi.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        texttgltransaksi.setEnabled(false);

        textkodejasatransaksi.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textkodejasatransaksi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textkodejasatransaksiKeyTyped(evt);
            }
        });

        textnamajasatransaksi.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textnamajasatransaksi.setEnabled(false);

        textsatuantransaksi.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textsatuantransaksi.setEnabled(false);

        texthargatransaksi.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        texthargatransaksi.setEnabled(false);

        textnoordertransaksi.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textnoordertransaksi.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textnoordertransaksiKeyTyped(evt);
            }
        });

        textjumlahtransaksi.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textjumlahtransaksi.setEnabled(false);

        texttotal.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        texttotal.setEnabled(false);

        textbayar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textbayar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                textbayarKeyTyped(evt);
            }
        });

        textkembali.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        textkembali.setEnabled(false);

        btnSimpanT.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnSimpanT.setText("Simpan");
        btnSimpanT.setContentAreaFilled(false);
        btnSimpanT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanTActionPerformed(evt);
            }
        });

        btnTambahT.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnTambahT.setText("Tambah");
        btnTambahT.setContentAreaFilled(false);
        btnTambahT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahTActionPerformed(evt);
            }
        });

        btnUbahT.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnUbahT.setText("Ubah");
        btnUbahT.setContentAreaFilled(false);
        btnUbahT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUbahTActionPerformed(evt);
            }
        });

        btnHapusT.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnHapusT.setText("Hapus");
        btnHapusT.setContentAreaFilled(false);
        btnHapusT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusTActionPerformed(evt);
            }
        });

        btnCariT.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnCariT.setText("Cari");
        btnCariT.setContentAreaFilled(false);
        btnCariT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariTActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                        .addComponent(jLabel25)
                                        .addGap(18, 18, 18))
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(jLabel26)
                                        .addGap(55, 55, 55)))
                                .addGroup(jPanel4Layout.createSequentialGroup()
                                    .addComponent(jLabel24)
                                    .addGap(43, 43, 43)))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel27)
                                    .addComponent(jLabel28))
                                .addGap(48, 48, 48)))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(textnotransaksi)
                                .addComponent(texttgltransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(textkodejasatransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(textsatuantransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(textnamajasatransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel29)
                        .addGap(147, 147, 147)
                        .addComponent(texthargatransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(48, 48, 48)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnSimpanT)
                        .addGap(18, 18, 18)
                        .addComponent(btnTambahT)
                        .addGap(18, 18, 18)
                        .addComponent(btnUbahT)
                        .addGap(18, 18, 18)
                        .addComponent(btnHapusT)
                        .addGap(18, 18, 18)
                        .addComponent(btnCariT))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel30)
                            .addComponent(jLabel31)
                            .addComponent(jLabel32))
                        .addGap(44, 44, 44)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textjumlahtransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(texttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textnoordertransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel33)
                            .addComponent(jLabel34))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textkembali, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textbayar, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(81, 81, 81)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel24)
                            .addComponent(textnotransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel30)
                            .addComponent(textnoordertransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel25)
                            .addComponent(texttgltransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel31)
                            .addComponent(textjumlahtransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel26)
                            .addComponent(textkodejasatransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel32)
                            .addComponent(texttotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel27)
                            .addComponent(textnamajasatransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel33)
                            .addComponent(textbayar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel28)
                            .addComponent(textsatuantransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel34)
                            .addComponent(textkembali, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel29)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(texthargatransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnSimpanT)
                                .addComponent(btnTambahT)
                                .addComponent(btnUbahT)
                                .addComponent(btnHapusT)
                                .addComponent(btnCariT))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(164, 164, 164)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 111, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Transaksi", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 835, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbltgl, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblwaktu, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblwaktu, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbltgl, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 455, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        // TODO add your handling code here:
        setStatusVisible(false);
    }//GEN-LAST:event_formInternalFrameClosing

    private void formInternalFrameIconified(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameIconified
        // TODO add your handling code here:
        setStatusVisible(true);
    }//GEN-LAST:event_formInternalFrameIconified

    private void btnUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUbahActionPerformed
        // TODO add your handling code here:
        ubah();
    }//GEN-LAST:event_btnUbahActionPerformed

    private void textkodepelangganKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textkodepelangganKeyTyped
        // TODO add your handling code here:
        if(evt.getKeyChar() == KeyEvent.VK_ENTER) {
            tambah();
            textnamapelanggan.requestFocus();
        }
    }//GEN-LAST:event_textkodepelangganKeyTyped

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        // TODO add your handling code here:
        savePelanggan();
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void btnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariActionPerformed
        // TODO add your handling code here:
        cari();
    }//GEN-LAST:event_btnCariActionPerformed

    private void btnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahActionPerformed
        // TODO add your handling code here:
        tambah();
        textnamapelanggan.requestFocus();
    }//GEN-LAST:event_btnTambahActionPerformed

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        // TODO add your handling code here:
        hapus();
    }//GEN-LAST:event_btnHapusActionPerformed

    private void cmbnamajasaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbnamajasaActionPerformed
        // TODO add your handling code here:
        if(cmbnamajasa.getSelectedIndex()==0){
            textharga.setText("2000");
        }else if(cmbnamajasa.getSelectedIndex()==1){
            textharga.setText("4000");
        } else if(cmbnamajasa.getSelectedIndex()==2){
            textharga.setText("6000");
        }
    }//GEN-LAST:event_cmbnamajasaActionPerformed

    private void cmbsatuanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbsatuanActionPerformed
        // TODO add your handling code here:
        if(cmbsatuan.getSelectedIndex()==1){
            textharga.setText("1000");
        }else if(cmbsatuan.getSelectedIndex()==1){
            textharga.setText("3000");
        }else if(cmbsatuan.getSelectedIndex()==1){
            textharga.setText("5000");
        }
    }//GEN-LAST:event_cmbsatuanActionPerformed

    private void btnSimpanJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanJActionPerformed
        // TODO add your handling code here:
        saveJasa();
    }//GEN-LAST:event_btnSimpanJActionPerformed

    private void btnTambahJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahJActionPerformed
        // TODO add your handling code here:
        tambahJasa();
    }//GEN-LAST:event_btnTambahJActionPerformed

    private void btnUbahJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUbahJActionPerformed
        // TODO add your handling code here:
        ubahJasa();
    }//GEN-LAST:event_btnUbahJActionPerformed

    private void btnHapusJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusJActionPerformed
        // TODO add your handling code here:
        hapusJasa();
    }//GEN-LAST:event_btnHapusJActionPerformed

    private void btnCariJActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariJActionPerformed
        // TODO add your handling code here:
        cariJasa();
    }//GEN-LAST:event_btnCariJActionPerformed

    private void textkodepelangganorderKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textkodepelangganorderKeyTyped
        // TODO add your handling code here:
        if(evt.getKeyChar() == KeyEvent.VK_ENTER) {
            cariPelanggan();
        }
    }//GEN-LAST:event_textkodepelangganorderKeyTyped

    private void cmbkodepewangiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbkodepewangiActionPerformed
        // TODO add your handling code here:
        if(cmbkodepewangi.getSelectedIndex()==0){
            textpewangi.setText("Mawar");
        }else if(cmbkodepewangi.getSelectedIndex()==1){
            textpewangi.setText("Melati");
        } else if(cmbkodepewangi.getSelectedIndex()==2){
            textpewangi.setText("Lavender");
        }
    }//GEN-LAST:event_cmbkodepewangiActionPerformed

    private void btnSimpanOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanOActionPerformed
        // TODO add your handling code here:
        saveOrder();
    }//GEN-LAST:event_btnSimpanOActionPerformed

    private void btnTambahOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahOActionPerformed
        // TODO add your handling code here:
        tambahOrder();
    }//GEN-LAST:event_btnTambahOActionPerformed

    private void btnUbahOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUbahOActionPerformed
        // TODO add your handling code here:
        ubahOrder();
    }//GEN-LAST:event_btnUbahOActionPerformed

    private void btnHapusOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusOActionPerformed
        // TODO add your handling code here:
        hapusOrder();
    }//GEN-LAST:event_btnHapusOActionPerformed

    private void btnCariOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariOActionPerformed
        // TODO add your handling code here:
        cariOrder();
    }//GEN-LAST:event_btnCariOActionPerformed

    private void textkodejasatransaksiKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textkodejasatransaksiKeyTyped
        // TODO add your handling code here:
        if(evt.getKeyChar() == KeyEvent.VK_ENTER) {
            cariKodeJasa();
        }
    }//GEN-LAST:event_textkodejasatransaksiKeyTyped

    private void textnoordertransaksiKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textnoordertransaksiKeyTyped
        // TODO add your handling code here:
        if(evt.getKeyChar() == KeyEvent.VK_ENTER) {
            cariNoOrder();
            hitungTotal();
            textbayar.requestFocus();
        }
    }//GEN-LAST:event_textnoordertransaksiKeyTyped

    private void textbayarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_textbayarKeyTyped
        // TODO add your handling code here:
        if(evt.getKeyChar() == KeyEvent.VK_ENTER) {
            hitungKembali();
            btnSimpanT.requestFocus();
        }
    }//GEN-LAST:event_textbayarKeyTyped

    private void btnSimpanTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanTActionPerformed
        // TODO add your handling code here:
        saveTransaksi();
    }//GEN-LAST:event_btnSimpanTActionPerformed

    private void btnTambahTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahTActionPerformed
        // TODO add your handling code here:
        tambahTransaksi();
    }//GEN-LAST:event_btnTambahTActionPerformed

    private void btnUbahTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUbahTActionPerformed
        // TODO add your handling code here:
        ubahTransaksi();
    }//GEN-LAST:event_btnUbahTActionPerformed

    private void btnHapusTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusTActionPerformed
        // TODO add your handling code here:
        hapusTransaksi();
    }//GEN-LAST:event_btnHapusTActionPerformed

    private void btnCariTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariTActionPerformed
        // TODO add your handling code here:
        cariTransaksi();
    }//GEN-LAST:event_btnCariTActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCari;
    private javax.swing.JButton btnCariJ;
    private javax.swing.JButton btnCariO;
    private javax.swing.JButton btnCariT;
    private javax.swing.JButton btnHapus;
    private javax.swing.JButton btnHapusJ;
    private javax.swing.JButton btnHapusO;
    private javax.swing.JButton btnHapusT;
    private javax.swing.JButton btnSimpan;
    private javax.swing.JButton btnSimpanJ;
    private javax.swing.JButton btnSimpanO;
    private javax.swing.JButton btnSimpanT;
    private javax.swing.JButton btnTambah;
    private javax.swing.JButton btnTambahJ;
    private javax.swing.JButton btnTambahO;
    private javax.swing.JButton btnTambahT;
    private javax.swing.JButton btnUbah;
    private javax.swing.JButton btnUbahJ;
    private javax.swing.JButton btnUbahO;
    private javax.swing.JButton btnUbahT;
    private javax.swing.JComboBox cmbkodepewangi;
    private javax.swing.JComboBox cmbnamajasa;
    private javax.swing.JComboBox cmbsatuan;
    private javax.swing.JComboBox cmbstatus;
    private com.toedter.calendar.JDateChooser dtTglAkhir;
    private com.toedter.calendar.JDateChooser dtTglAwal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lbltgl;
    private javax.swing.JLabel lblwaktu;
    private javax.swing.JTextField textJumlah;
    private javax.swing.JTextField textNoOrder;
    private javax.swing.JTextArea textalamat;
    private javax.swing.JTextField textbayar;
    private javax.swing.JTextField textharga;
    private javax.swing.JTextField texthargatransaksi;
    private javax.swing.JTextField textjumlahtransaksi;
    private javax.swing.JTextField textkembali;
    private javax.swing.JTextField textkodejasa;
    private javax.swing.JTextField textkodejasatransaksi;
    private javax.swing.JTextField textkodepelanggan;
    private javax.swing.JTextField textkodepelangganorder;
    private javax.swing.JTextField textnamajasatransaksi;
    private javax.swing.JTextField textnamapelanggan;
    private javax.swing.JTextField textnamapelangganorder;
    private javax.swing.JTextField textnoordertransaksi;
    private javax.swing.JTextField textnotelp;
    private javax.swing.JTextField textnotransaksi;
    private javax.swing.JTextField textpewangi;
    private javax.swing.JTextField textsatuantransaksi;
    private javax.swing.JTextField texttgltransaksi;
    private javax.swing.JTextField texttotal;
    // End of variables declaration//GEN-END:variables
}
